import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { LandingPageLayout } from '../layouts/LandingPage/LandingPageLayout';
import { AboutView } from '../views/AboutView';
import { HomeView } from '../views/HomeView';
import { ServicesView } from '../views/Services';

export const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<LandingPageLayout />}>
          <Route  path="/" element={<HomeView />} />
          <Route path="sobre" element={<AboutView />} />
          <Route path="servicos" element={<ServicesView />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};
