import styled from "styled-components";

export const Container = styled.section`
  background-image: linear-gradient(
    to right,
    var(--secondary-color),
    var(--primary-color)
  );
  width: 100%;
  min-height: 400px;
  padding: 50px;
`;
