import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from "react-hook-form";
import * as yup from 'yup';
import { Button } from "../shared/Button";
import { FormGroup } from "../shared/FormGroup";
import { Title } from "../shared/Title";
import * as S from "./styles";

export const Subscription = () => {
  const schema = yup.object().shape({
    name: yup.string().min(10, 'Este campo deve ter ao menos 10 caracteres').required('O campo Nome é obrigatório'),
    email: yup.string().email('Digite um email válido').required('O campo Email é obrigatório'),
  })

  const { register, handleSubmit, formState:{ errors } } = useForm({
    resolver: yupResolver(schema)
  });


  const newLead = (data) => {
    console.log(data)
  };

  return (
    <S.Container>
      <Title size={45} color="white" align="center"> Quer receber promoções especiais?</Title>

      <Title size={28} color="white" align="center"> Inscreva-se </Title>

      <form className="d-flex flex-column align-items-center" onSubmit={handleSubmit(newLead)}>


        <div className="w-50">
          <FormGroup color="white" errors={errors.name}>
            <label htmlFor="name">Seu nome</label>
            <input type="text" name="name" {...register('name')} />
          </FormGroup>
        </div>
        <div className="w-50">
          <FormGroup color="white" errors={errors.email}>
            <label htmlFor="email">Seu email</label>
            <input type="text" name="email" {...register('email')}/>
          </FormGroup>
        </div>

        <Button size={30} title="Se Inscrever" style={{marginTop: '30px'}}>Se Inscrever</Button>


      </form>

    </S.Container>
  );
};
