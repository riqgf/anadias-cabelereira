import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  > * {
    width: 50% !important;
    padding: 0 20px;
  }
`
