import ana from "../../assets/images/ana.png";
import { Text } from "../shared/Text";
import { Title } from "../shared/Title";
import * as S from "./styles";
export const AboutProfessional = () => {
  return (
    <>
      <S.Container className="d-flex">
        <div>
        <Title size={35}>Sobre a profissional</Title>
          <Text size={25}>Olá, meu nome é Ana Cláudia.</Text>
          <Text size={20}>
           Sou uma cabelereira que trabalha há mais de 18 anos nesse ramo da beleza. Possui diversas
            certificações, principalmente na parte de Colorimetria.
          </Text>
          <Text size={20}>
            Minha carreira começou com o corte de cabelo, mas logo me descobri apaixonada por coloração, me especializando
            principalmente nos ruivos.
          </Text>
        </div>
        <img className="" src={ana} alt="Ana Claudia" />
      </S.Container>
    </>
  );
};
