import styled from "styled-components";

const ButtonStyled = styled.button`
    font-size: ${props => props.size || 16}px;
    background-color: var(--${props => props.color || 'primary'}-color);
    color: white;
    text-align: center;
    border: 0px;
    padding: 7px 10px;
    font-weight: bold;
    cursor: pointer;

    &:is(:focus, :active, :hover) {
        filter: brightness(110%);
    }
`;


export const Button = (props) => {
    return (
        <ButtonStyled {...props}>
            {props.children}
        </ButtonStyled>
    )
}