import styled from "styled-components";

const TitleStyled = styled.h3`
    font-size: ${props => props.size || 24}px;
    color: var(--${props => props.color || 'secondary'}-color);
    text-align: ${props => props.align || 'left'};
`;


export const Title = (props) => {
    return (
        <TitleStyled {...props}>
            {props.children}
        </TitleStyled>
    )
}