import styled from "styled-components";

const FormGroupStyled = styled.div`
    font-size: ${props => props.size || 18}px;
    color: var(--${props => props.color || 'dark'}-color);
    margin: 20px 0;
    width: 100%;
    margin: auto;
    margin-top: 15px;


    
    label {
        display: block;
        font-weight: 500;
    }
    input{
        padding: 7px 10px;
        border-radius: 8px;
        box-shadow: 0px inset;
        font-size: ${props => props.size || 18}px;
        border: 2px solid var(--${props => props.errors?.message ? 'danger' : 'gray'}-color);
        width: 100%;
        &, &:focus {
            outline: none;
        }
        &:focus {
            border-color: var(--${props => props.errors?.message ? 'danger' : 'primary'}-color);
        }
        
    }
    .error{
        color: var(--danger-color);
    }
`;


export const FormGroup = (props) => {
    return (
        <FormGroupStyled {...props}>
            {props.children}
            <small className="error">{ props.errors?.message }</small>
        </FormGroupStyled>
    )
}