import styled from "styled-components";

const TextStyled = styled.h3`
    font-size: ${props => props.size || 24}px;
    color: var(--${props => props.color || 'dark'}-color);
    margin: 30px 0;
`;


export const Text = ({ children, size, color }) => {
    return (
        <TextStyled size={size}>
            {children}
        </TextStyled>
    )
}
