import styled from "styled-components";

export default styled.img`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 250px;
  width: 100%;
  background-color: var(--primary-color);
  color: #fff;
  margin: 15px;
  font-size: 4em;
  object-fit: cover;
  object-position: 80%;
`;
