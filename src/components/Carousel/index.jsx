import React, { useState } from "react";
import ReactElasticCarousel from "react-elastic-carousel";
import slide1 from '../../assets/images/carousel/slide1.jpg';
import slide2 from '../../assets/images/carousel/slide2.jpg';
import Item from "./item";


export const Carousel = () => {
  const breakPoints = [
    { width: 1, itemToShow: 1 },
    { width: 550, itemToShow: 2, itemToScroll: 2 },
    { width: 768, itemToShow: 3 },
    { width: 1200, itemToShow: 4 },
  ];
  const [items, setitems] = useState([
    slide1, slide2
  ]);

  const addItems = () => {
    const nextItem = Math.max(1, items.length + 1);
    setitems([...items, nextItem])
  }

  const removeItem = () => {
    const endRange = Math.max(0, items.length - 1);
    setitems(items.slice(0, endRange))
  }

  return (
    <div className="carousel__container">
      <div className="carousel__controls"></div>
      <div className="carousel__content">
        <ReactElasticCarousel breakPoints={breakPoints}>
        {items.map((item) => (
            <Item key={item} src={item} />
          ))}
        </ReactElasticCarousel>
      </div>
    </div>
  );
};
