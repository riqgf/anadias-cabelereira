import { Router } from "./router"


import './assets/styles/global.css'
import './assets/styles/theme.css'


function App() {

  return (
    <Router />
  )
}

export default App
