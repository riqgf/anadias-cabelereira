import React from "react";
import styled from "styled-components";
import cut from "../assets/images/services/cut.jpg";
import escova from "../assets/images/services/escova.jpg";
import manicure from "../assets/images/services/manicure.jpg";
import paint from "../assets/images/services/paint.jpg";
import pedicure from "../assets/images/services/pedicure.jpg";

const ContainerService = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const Service = styled.img`
  width: 33%;
  height: 250px;
  object-fit: cover;
  object-position: center;
  box-shadow: 0 0 17px rgb(0, 0, 0, 0.1);
  position: relative;
  z-index: 9;
  transition: 1s;
  &:hover {
    z-index: 10;
    transition: 1s;
    transform: scale(1.2);
  }
`;

export const ServicesView = () => {
  const services = [
    { name: "Corte de cabelo", img: cut },
    { name: "Pintura de cabelo", img: paint },
    { name: "Pedicure", img: pedicure },
    { name: "Manicure", img: manicure },
    { name: "Escova", img: escova },
  ];

  return (
    <div className="container">
      <ContainerService>
        {services.map((service) => (
          <Service src={service.img} title={ service.name }/>
        ))}
      </ContainerService>
    </div>
  );
};
