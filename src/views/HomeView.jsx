import React from 'react'
import { AboutProfessional } from '../components/AboutProfessional'
import { Carousel } from '../components/Carousel'
import { Subscription } from '../components/Subscription'

export const HomeView = () => {
  return (
    <div>
      <Carousel />

      <div className="container">
        <AboutProfessional />
      </div>
      <Subscription />
      
    </div>
  )
}
