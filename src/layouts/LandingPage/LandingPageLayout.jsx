import React from 'react';
import { Outlet } from 'react-router-dom';
import { Navbar } from './partials/Navbar';
export const LandingPageLayout = () => {
  return (
    <>
      <Navbar />
      <main>
      <Outlet />

      </main>
    </>
  );
};
