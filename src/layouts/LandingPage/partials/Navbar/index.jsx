import React, { useEffect, useState } from "react";
import * as S from "./styles";

export const Navbar = () => {
  const [links, setlinks] = useState([
    { url: "/", label: "Inicio" },
    { url: "/sobre", label: "Sobre" },
    { url: "/servicos", label: "Serviços" },
  ]);

  // Check Scroll
  const [positionHeader, setPositionHeader] = useState(0);
  const handleScroll = () => {
    const position = window.pageYOffset;
    const heightNavbar = document.querySelector('header').clientHeight;
    setPositionHeader(position > heightNavbar ? 'fixed' : 'relative');
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);


  useEffect(() => {
      console.log('olá')
  }, []);



  return (
    <S.Header position={positionHeader}>
      <S.NavLink to="/">
        <h1>Anadias</h1>
      </S.NavLink>

      <nav>
        {links.map((link) => (
          <S.NavLink key={link.url} to={link.url}>{link.label}</S.NavLink>
        ))}
      </nav>
    </S.Header>
  );
};
