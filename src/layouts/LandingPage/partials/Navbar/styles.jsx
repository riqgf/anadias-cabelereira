import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const Header = styled.header`
  transition: 2s;
  display: flex;
  justify-content: space-between;
  width: 100%;
  padding: 10px 20px;
  box-shadow: 0 0 19px rgb(0, 0, 0, 0.1);
  position: ${props => props.position || 'relative'};
  top: 0;
  left: 0;
  z-index: 10;
  background-color: var(--white-color);

  h1 {
    font-size: 24px;
    text-align: left;
  }
  nav {
    align-self: center;
    width: auto;
    display: flex;
    a {
      padding: 10px 30px;
      font-size: 18px;

      &:hover {
        background-color: var(--secondary-color);
        color: white;
        transition: 0.3s;
      }
    }
  }
`;

export const NavLink = styled(Link)`
  padding: 10px;
  font-weight: 500;
  color: var(--secondary-color);
  text-decoration: none;
  text-align: center;
  border-radius: 12px;
`;
